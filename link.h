#pragma once

#include "list.h"

enum link_direction
{
	LINK_DIRECTION_DOWN,
	LINK_DIRECTION_UP
};

struct link_port
{
	struct list list;

	char *name;
	enum link_direction direction;
	struct type const *type;
};

struct link
{
	struct list list;

	struct list ports;
};

struct link_port *create_link_port(
		char *name,
		enum link_direction,
		struct type const *type);
