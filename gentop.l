%option never-interactive
%option 8bit
%option noinput
%option nounput
%option noyywrap
%option nodefault
%option reentrant
%option yylineno
%option bison-bridge
%option bison-locations
%option extra-type="struct context *"

%top{
#include "gentop_parse.h"

#define YY_USER_INIT \
	yylloc->last_line = 1; \
	yylloc->last_column = 0;

#define YY_USER_ACTION \
	yycolumn += yyleng; \
	yylloc->first_line = yylloc->last_line; \
	yylloc->first_column = yylloc->last_column + 1; \
	yylloc->last_line = yylineno; \
	yylloc->last_column = yycolumn;
}

INTEGER		(0|[1-9][0-9]*)
IDENTIFIER	[A-Z_a-z][0-9A-Z_a-z]*

%%

\t		/* ignore */
\n		yylloc->last_column = yycolumn = 0; /* ignore */
\ 		/* ignore */

\(		return '(';	// 28
\)		return ')';	// 29
:		return ':';	// 3a
;		return ';';	// 3b

down		return DOWN;
downto		return DOWNTO;
end		return END;
is		return IS;
link		return LINK;
to		return TO;
up		return UP;

{INTEGER}	{
			char buffer[yyleng+1];
			memcpy(buffer, yytext, yyleng);
			buffer[yyleng] = '\0';
			yylval->integer = atoi(buffer);
			/// @todo overflow
			return INTEGER;
		}

{IDENTIFIER}	{
			yylval->identifier = strndup(yytext, yyleng);
			return IDENTIFIER;
		}

.		{
			fprintf(stderr,
				"%d:%d:Invalid character %02x\n",
				yylloc->first_line, yylloc->first_column,
				yytext[0]);
			return YYerror;
		}

