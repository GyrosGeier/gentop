FROM debian
RUN [ "apt-get", "update" ]
RUN [ "apt-get", "--yes", "install", "autoconf", "autoconf-archive", "automake", "bison", "flex", "gcc", "make", "valgrind" ]
