#pragma once

#include "type.h"
#include "link.h"

#include "list.h"

struct context
{
	struct list types;
	struct list links;
};
