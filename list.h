#pragma once

#include <stddef.h>
#include <stdint.h>

#include <assert.h>

/** @page lists
 *
 * Lists come in two flavours: circular and null-terminated.
 */

#define container_of(p, t, m) \
	((t *)(((uintptr_t)(p)) - offsetof(t, m)))

/** list node, common to circular and null-terminated. */
struct list
{
	struct list *next, *prev;
};

/* Initialize a null-terminated list */
static inline void nlist_init(
		struct list **const list)
{
	assert(list != NULL);
	*list = NULL;
}

/* Insert into a null-terminated list, after the element the list head points
 * at.
 */
static inline void nlist_insert_after(
		struct list **const list,
		struct list *const element)
{
	assert(list != NULL);
	assert(element != NULL);

	element->prev = (*list);
	if(*list)
	{
		element->next = (*list)->next;
		(*list)->next = element;
		if(element->next)
			element->next->prev = element;
	}
	else
		element->next = NULL;

	*list = element;
}

static inline void nlist_rewind(
		struct list **const list)
{
	struct list *i;

	assert(list != NULL);

	i = *list;
	if(!i)
		return;

	while(i->prev)
		i = i->prev;
	*list = i;
}

/* Initialize a circular list */
static inline void clist_init(
		struct list *const list)
{
	assert(list != NULL);
	list->next = list->prev = list;
}

/** Move the head of a circular list */
static inline void clist_move_head(
		struct list *const new_head,
		struct list *const old_head)
{
	assert(new_head != NULL);
	assert(old_head != NULL);
	if(new_head == old_head)
		return;

	assert(old_head->next != NULL);
	assert(old_head->prev != NULL);

	new_head->next = old_head->next;
	new_head->prev = old_head->prev;
	new_head->next->prev = new_head;
	new_head->prev->next = new_head;
}

/* Insert into a circular list, after an element */
static inline void clist_insert_after(
		struct list *const after,
		struct list *const element)
{
	assert(after != NULL);
	assert(element != NULL);
	assert(after->next != NULL);

	element->next = after->next;
	element->prev = after;
	after->next->prev = element;
	after->next = element;
}

/* Insert into a circular list, before an element */
static inline void clist_insert_before(
		struct list *const before,
		struct list *const element)
{
	assert(before != NULL);
	assert(element != NULL);
	assert(before->next != NULL);

	element->next = before;
	element->prev = before->prev;
	before->prev->next = element;
	before->prev = element;
}

/* Insert into a circular list */
static inline void clist_insert(
		struct list *const before,
		struct list *const element)
{
	assert(before != NULL);
	assert(element != NULL);
	assert(before->next != NULL);

	element->prev = before;
	element->next = before->next;
	before->next->prev = element;
	before->next = element;
}
