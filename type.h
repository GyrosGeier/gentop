#pragma once

#include "list.h"
#include "range.h"

struct context;

struct type
{
	struct list list;

	char *name;
	enum
	{
		SIMPLE_TYPE,
		ARRAY_TYPE
	} kind;
	union
	{
		struct range range;
	};
};

struct type *add_simple_type(
		struct context *ctx,
		char *name);
struct type *add_array_type(
		struct context *ctx,
		char *name,
		struct range range);
