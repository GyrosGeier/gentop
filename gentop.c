#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "gentop_parse.h"
#include "gentop_lex.h"

int main(int argc, char **argv)
{
	int rc = 0;

	unsigned int count = 0;

	struct context ctx;
	clist_init(&ctx.types);
	clist_init(&ctx.links);

	for(int i = 1; i < argc; ++i)
	{
		yyscan_t scanner;
		FILE *in = fopen(argv[i], "r");

		if(!in)
		{
			rc = 1;
			continue;
		}

		yylex_init_extra(&ctx, &scanner);
		yyset_in(in, scanner);
		if(yyparse(scanner) != 0)
		{
			rc = 1;
		}

		yylex_destroy(scanner);
		fclose(in);
	}

	for(struct list *next, *i = ctx.types.next; i != &ctx.types; i = next)
	{
		next = i->next;
		struct type *t = container_of(i, struct type, list);
		free(t->name);
		free(t);
		++count;
	}

	return rc;
}
