#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "link.h"

#include <stdlib.h>

struct link_port *create_link_port(
		char *const name,
		enum link_direction const direction,
		struct type const *const type)
{
	struct link_port *const ret = malloc(sizeof *ret);
	ret->name = name;
	ret->direction = direction;
	ret->type = type;
	return ret;
}
