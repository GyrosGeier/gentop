#pragma once

enum range_direction
{
	RANGE_DIRECTION_DOWN,
	RANGE_DIRECTION_UP
};

struct range
{
	int left, right;
	enum range_direction direction;
};
