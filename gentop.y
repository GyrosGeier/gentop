%locations
%define api.pure full
%define parse.error verbose
%param {yyscan_t scanner}

%code requires {
typedef void *yyscan_t;
#include "gentop.h"
}

%{
#include "gentop_lex.h"

void yyerror(
		YYLTYPE *yylloc,
		yyscan_t scanner,
		char const *const msg)
{
	(void)scanner;
	fprintf(stderr,
		"%d:%d-%d:%d:%s\n",
		yylloc->first_line, yylloc->first_column,
		yylloc->last_line, yylloc->last_column,
		msg);
}
%}

%union {
	int integer;
	char *identifier;

	enum link_direction link_direction;
	enum range_direction range_direction;

	struct range range;

	/* pointer to first element of headless list */
	struct list *link_ports;
	struct link_port *link_port;
	struct type *type;
}

%destructor { free($$); } <identifier> <link_port>

%token DOWN "down"
%token DOWNTO "downto"
%token END "end"
%token IS "is"
%token LINK "link"
%token TO "to"
%token UP "up"

%token<integer> INTEGER
%token<identifier> IDENTIFIER

%type<link_direction> link_direction
%type<range_direction> range_direction

%type<range> range

%type<link_ports> link_ports
%type<link_port> link_port
%type<type> type

%%

declarations:
	declarations declaration
|	%empty

declaration:
	link_declaration

link_declaration:
	"link" IDENTIFIER "is" link_ports "end" "link" ';'
		{
			free($2);
			nlist_rewind(&$4);
			for(struct list *next, *i = $4; i; i = next)
			{
				next = i->next;
				struct link_port *const p = container_of(i, struct link_port, list);
				free(p->name);
				free(p);
			}
		}

link_ports:
	link_ports ';' link_port
		{
			$$ = $1;
			nlist_insert_after(&$$, &$3->list);
		}
|	link_port
		{
			nlist_init(&$$);
			nlist_insert_after(&$$, &$1->list);
		}

link_port:
	IDENTIFIER ':' link_direction type
		{ $$ = create_link_port($1, $3, $4); }

/*
	identifier_list ':' link_direction type

identifier_list:
	identifier_list ',' IDENTIFIER
		{ free($3); }
|	IDENTIFIER
		{ free($1); }
 */

link_direction:
	"down"
		{ $$ = LINK_DIRECTION_DOWN; }
|	"up"
		{ $$ = LINK_DIRECTION_UP; }

type:
	IDENTIFIER
		{
			$$ = add_simple_type(yyget_extra(scanner), $1);
		}
|	IDENTIFIER range
		{
			$$ = add_array_type(yyget_extra(scanner), $1, $2);
		}

range:
	'(' INTEGER range_direction INTEGER ')'
		{
			$$.left = $2;
			$$.right = $4;
			$$.direction = $3;
		}

range_direction:
	"downto"
		{ $$ = RANGE_DIRECTION_DOWN; }
|	"to"
		{ $$ = RANGE_DIRECTION_UP; }
