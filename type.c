#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "type.h"

#include "gentop.h"

#include <stdlib.h>
#include <string.h>

static struct type *find_simple_type(
		struct context *const ctx,
		char const *const name)
{
	for(struct list *i = ctx->types.next; i != &ctx->types; i = i->next)
	{
		struct type *const t = container_of(i, struct type, list);
		if(!strcmp(name, t->name))
		{
			if(t->kind != SIMPLE_TYPE)
				assert("array spec missing");	/// @todo
			return t;
		}
	}
	return NULL;
}

struct type *add_simple_type(
		struct context *const ctx,
		char *const name)
{
	struct type *const existing = find_simple_type(ctx, name);
	if(existing)
	{
		free(name);
		return existing;
	}

	struct type *const ret = malloc(sizeof *ret);
	ret->name = name;
	ret->kind = SIMPLE_TYPE;
	clist_insert(&ctx->types, &ret->list);
	return ret;
}

static struct type *find_array_type(
		struct context *const ctx,
		char const *const name,
		struct range const range)
{
	for(struct list *i = ctx->types.next; i != &ctx->types; i = i->next)
	{
		struct type *const t = container_of(i, struct type, list);
		if(!strcmp(name, t->name))
		{
			if(t->kind != ARRAY_TYPE)
				assert("array spec on simple type");	/// @todo
			if(t->range.left != range.left)
				continue;
			if(t->range.right != range.right)
				continue;
			if(t->range.direction != range.direction)
				continue;
			return t;
		}
	}
	return NULL;
}

struct type *add_array_type(
		struct context *const ctx,
		char *const name,
		struct range const range)
{
	struct type *const existing = find_array_type(ctx, name, range);
	if(existing)
	{
		free(name);
		return existing;
	}

	struct type *ret = malloc(sizeof *ret);
	ret->name = name;
	ret->kind = ARRAY_TYPE;
	ret->range = range;
	clist_insert(&ctx->types, &ret->list);
	return ret;
}
